# -*- coding: utf-8 -*-
import cv2
import sys
import os
import re
import subprocess
import numpy as np
from numpy import zeros

from PIL import Image
import scipy.misc as sc

import matplotlib

from matplotlib import image
from matplotlib import pyplot

def inverteImagem(imagemEntrada):
    imagemSaida = zeros([len(imagemEntrada),len(imagemEntrada[0])]);
    imagemSaida.fill(0)
    h = len(imagemEntrada)
    w = len(imagemEntrada[0])
    
    for y in range(h):
        for x in range(w):
                imagemSaida[y,x] = int(255 - imagemEntrada[y,x]);
    
    return imagemSaida

def pgmread(filename):
    f = open(filename,'r')
    line = f.readline()
    magicNum = line.strip()
    line = f.readline()
    [largura, altura] = (line.strip()).split()
    largura = int(largura)
    altura = int(altura)
    img = []
    buf = f.read()
    lista = buf.split()
    npsaida = np.zeros((altura, largura), dtype=np.int)

    i = 0
    j = 0
    # Esse for recebe os valores vindos do arquivo e monta um numpy
    # que é o equivalente a imagem e tem o mesmo numero de linhas
    # e colunas lido do arquivo pbm
    for elem in lista:
        for e in elem:
            if (i >= altura):
                j = 0;
                i+=1;
                break;
            elif (j >= largura-1):
                j = 0;
                i+=1;
                break;
            
            j+=1;
            npsaida[i][j] = e;

    f.close();

    return (npsaida, largura, altura)

def geraArquivo(nome, largura, altura, matrizDados):
    arq = open(nome,'w')
    arq.write('r'); #inicializador
    arq.write(str(largura));
    arq.write('x'); #separador
    arq.write(str(altura));
    arq.write('x'); #fimcabecalho
    for elem in matrizDados:
        for e in elem:
            arq.write(str(e));
            
    arq.close();
    return arq

def geraArquivoUnico(nome, largura, altura, matrizCian, matrizMagenta, matrizAmarela, matrizPreta):
    arq = open(nome,'w')
    arq.write('r'); #inicializador
    arq.write(str(largura));
    arq.write('x'); #separador
    arq.write(str(altura));
    
    arq.write('c'); #inicio do Cian
    for elem in matrizCian:
        for e in elem:
            arq.write(str(e));

    arq.write('m'); #inicio do Magenta
    for elem in matrizMagenta:
        for e in elem:
            arq.write(str(e));

    arq.write('y'); #inicio do Magenta
    for elem in matrizAmarela:
        for e in elem:
            arq.write(str(e));

    arq.write('k'); #inicio do Magenta
    for elem in matrizPreta:
        for e in elem:
            arq.write(str(e));


    arq.close();
    return arq
    

def convertplain(caminhoCompleto):
    
    comandoPlain = "pnmtoplainpnm " + caminhoCompleto + "/"
    comandoPlainCian = comandoPlain + "cian.tiff.pbm > " + caminhoCompleto + "/cian.pbm";
    comandoPlainMagenta = comandoPlain + "magenta.tiff.pbm > " + caminhoCompleto + "/magenta.pbm";
    comandoPlainPreto = comandoPlain + "preto.tiff.pbm > " + caminhoCompleto + "/preto.pbm";
    comandoPlainAmarelo = comandoPlain + "amarelo.tiff.pbm > " + caminhoCompleto + "/amarelo.pbm";

    print(comandoPlainCian)

    os.system(comandoPlainCian); 
    os.system(comandoPlainMagenta);
    os.system(comandoPlainPreto); 
    os.system(comandoPlainAmarelo); 
    return


os.system("export CONVERTIO_API_KEY=89624b3a206cca0692de0caaf641bf8e");

#caminhoCompleto=sys.argv[1];

caminhoCompleto = input("Digite o caminho completo da imagem: ");
nomeImg=input("Digite o nome da imagem (com a extensão): ");

print(caminhoCompleto);

caminhoImagem = caminhoCompleto +"/" + nomeImg
print(caminhoImagem);


imgcmy = Image.open(caminhoImagem).convert('CMYK');
imgcmy.show();

print("Voce deseja redimensionar a imagem? \n");
opcao = input("Digite p para redimensionar em porcentagem, \n f para digitar uma largura fixa e \n n para deixar do tamanho atual: ");

if opcao == 'f':
    larguraBase = int(input("Digite a nova largura da imagem: "));
    porcentagem = (larguraBase/float(imgcmy.size[0]))
    altura = int((float(imgcmy.size[1])*float(porcentagem)))
    print(larguraBase)
    print(altura)
    img = imgcmy.resize((larguraBase,altura), Image.ANTIALIAS)
elif (opcao == 'p'):
    porcentagem = float(input("Digite a porcentagem de redimensionamento da imagem: "));
    novaBase = int((float(porcentagem/float(100)) * float(imgcmy.size[0])))
    novaAltura = int((float(porcentagem/float(100)) * float(imgcmy.size[1])))
    print(novaBase)
    print(novaAltura)
    img = imgcmy.resize((novaBase, novaAltura), Image.ANTIALIAS);
else: #qualquer outra opcao, deixa a imagem do mesmo tamanho
    img = imgcmy;
    
canais_num = np.array(img);

print("Separa canais");

(canalCian, canalMagenta, canalAmarelo, canalPreto) = cv2.split(canais_num)

print ("inverte magenta");
magenta = inverteImagem(canalMagenta)
print ("inverte amarelo");
amarelo = inverteImagem(canalAmarelo)
print ("inverte cian");
cian = inverteImagem(canalCian)
print ("inverte preto");
preto = inverteImagem(canalPreto)


print(canalMagenta)
print(magenta)

#matplotlib.image.imsave('../img/mocaricardo/separadoPy/magenta.tif', canalMagenta)
#matplotlib.image.imsave('../img/mocaricardo/separadoPy/yellow.tif', canalAmarelo)
#matplotlib.image.imsave('../img/mocaricardo/separadoPy/cian.tif', canalCian)
#matplotlib.image.imsave('../img/mocaricardo/separadoPy/preto.tif', canalPreto)

matplotlib.image.imsave(caminhoCompleto+'/magenta.tiff', magenta)
matplotlib.image.imsave(caminhoCompleto+'/amarelo.tiff', amarelo)
matplotlib.image.imsave(caminhoCompleto+'/cian.tiff', cian)
matplotlib.image.imsave(caminhoCompleto+'/preto.tiff', preto)

parametroConvertio = caminhoCompleto + "/*.tiff";

comandoConvertio = "convertio -f pbm -o "+ caminhoCompleto + "/ " + parametroConvertio;

os.system("export CONVERTIO_API_KEY=89624b3a206cca0692de0caaf641bf8e");

print(comandoConvertio)
os.system(comandoConvertio);

convertplain(caminhoCompleto);

nomepreto = caminhoCompleto + "/preto.pbm"; 
nomecian = caminhoCompleto + "/cian.pbm"; 
nomeamarelo = caminhoCompleto + "/amarelo.pbm"; 
nomemagenta = caminhoCompleto + "/magenta.pbm"; 

(nppreto, largPreto, alturaPreto) = np.array(pgmread(nomepreto))
(npcian, largCian, alturaCian) = np.array(pgmread(nomecian))
(npamarelo, largAmarelo, alturaAmarelo) = np.array(pgmread(nomeamarelo))
(npmagenta, largMagenta, alturaMagenta) = np.array(pgmread(nomemagenta))

print("preto normal")
print(npmagenta)

transpPreto = nppreto.transpose();
transpCian = npcian.transpose();
transpAmarelo = npamarelo.transpose();
transpMagenta = npmagenta.transpose();

print("Magenta transposta")
print(transpMagenta)

# ler todos os numeros do numpy e gerar o arquivo txt
#geraArquivo("preto.txt", largPreto, alturaPreto, transpPreto)
#geraArquivo("cian.txt", largCian, alturaCian, transpCian)
#geraArquivo("magenta.txt", largMagenta, alturaMagenta, transpMagenta)
#geraArquivo("amarelo.txt", largAmarelo, alturaAmarelo, transpAmarelo)

if ((largPreto == largCian) & (largMagenta == largAmarelo) & (largPreto == largAmarelo)): 
    largura = largCian;
else:
    print("Os arquivos não são correspondentes - larguras diferentes");

if (alturaPreto == alturaCian) & (alturaMagenta == alturaAmarelo) & (alturaPreto == alturaAmarelo): 
    altura = alturaCian;
else:
    print("Os arquivos não são correspondentes - alturas diferentes");

geraArquivoUnico("todosJuntos1.txt", largura, altura, transpCian, transpMagenta, transpAmarelo, transpPreto);



print("Prontinho!")
